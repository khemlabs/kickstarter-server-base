############################################################
# Dockerfile to build NodeJS 4 Installed Containers
# Based on Node:10
############################################################

FROM node:10

ARG LISTEN_PORT=3001

EXPOSE $LISTEN_PORT

# Install dependencies
RUN npm install -g pm2 \
	forever

CMD [/bin/bash]
